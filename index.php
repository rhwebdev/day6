<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Home</title>
   <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
</head>

<body>
   <div class="jumbotron">
      <h1 class="display-4">Hello, world!</h1>
      <p class="lead">This a simple dashboard.</p>
      <hr class="my-4">
      <a class="btn btn-primary btn-lg" href="add_user.html" role="button">Add User</a>
   </div>
</body>

</html>
