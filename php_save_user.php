<?php
header('Content-Type: application/json');
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo json_encode(array(
        'success' => false,
        'msg' => 'Method not acceptable',
    ));
    exit;
}

require_once 'handlers.php';
$checking = requestHandler($_POST);
if (sizeof($checking) > 0) {
    echo json_encode(array(
        'success' => false,
        'msg' => $checking,
    ));
    exit;
}

echo json_encode(array(
    'success' => true,
    'msg' => 'ok',
));
exit;
